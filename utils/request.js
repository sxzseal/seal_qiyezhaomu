// #ifdef H5
// h5端
let app = 'H5'
import Fly from "flyio/dist/npm/fly";
// #endif

// #ifdef APP-PLUS
// app端
let app = 'App'
import Fly from "flyio/dist/npm/wx";
// #endif

// #ifdef MP-WEIXIN
let app = 'WX'
import Fly from "flyio/dist/npm/wx";
// #endif

// import store from "./store";
// import { handleLoginFailure } from "@/utils";
import { VUE_APP_API_URL } from "@/config";
// import cookie from "@/utils/store/cookie";


const fly = new Fly()
fly.config.baseURL = VUE_APP_API_URL
uni.setStorage({
	key: 'appTpye',
	data: app
})


const defaultOpt = { login: true };

function baseRequest(options) {
//   // 结构请求需要的参数
  const { url, params, data, login, ...option } = options

  // 发起请求
  return fly.request(url, params || data, {
    ...option
  }).then(res => {
    const data = res.data || {};
    if (res.status !== 200) {
      return Promise.reject({ msg: "请求失败", res, data });
    }
    if ([401, 403].indexOf(data.status) !== -1) {
      // handleLoginFailure();
      return Promise.reject({ msg: res.data.msg, res, data, toLogin: true });
    } else if (data.code === 200 || data.code === "200" || data.status === "200") {
      return Promise.resolve(data, res);
    } else {
      return Promise.reject({ msg: res.data.msg, res, data });
    }
  });
}

/**
 * http 请求基础类
 * 参考文档 https://www.kancloud.cn/yunye/axios/234845
 *
 */
const request = ["post", "put", "patch"].reduce((request, method) => {
  /**
   *
   * @param url string 接口地址
   * @param data object get参数
   * @param options object axios 配置项
   * @returns {AxiosPromise}
   */
  request[method] = (url, data = {}, options = {}) => {
    return baseRequest(
      Object.assign({ url, data, method }, defaultOpt, options)
    );
  };
  return request;
}, {});

["get", "delete", "head"].forEach(method => {
  /**
   *
   * @param url string 接口地址
   * @param params object get参数
   * @param options object axios 配置项
   * @returns {AxiosPromise}
   */
  request[method] = (url, params = {}, options = {}) => {
    return baseRequest(
      Object.assign({ url, params, method }, defaultOpt, options)
    );
  };
});

export default request;
