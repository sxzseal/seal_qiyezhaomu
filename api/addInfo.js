import request from "@/utils/request";


/**
* 支付成功添加订单
*/
export function addInfo(data){
	return request.post('/info/add',data,{
		login: false
	})
}
