export default function MapLoader() {
	window._AMapSecurityConfig = {
		securityJsCode:'c8f394831896722b3f10dd4df42a5b20',
	}
	return new Promise((resolve, reject) => {
		if (window.AMap) {
			resolve(window.AMap);
		} else {
			var script = document.createElement('script');
			script.type = "text/javascript";
			script.async = true;
			script.src =
				"https://webapi.amap.com/maps?v=1.4.15&key=48deabf4994b44ff47acd2dcdfcdb916";
			script.onerror = reject;
			document.head.appendChild(script);
		}
		window.initAMap = () => {
			resolve(window.AMap);
		};
	});
}